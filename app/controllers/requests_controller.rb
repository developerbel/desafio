class RequestsController < ApplicationController
  def index
    @numbers = []

    extract

    transform
  end

  private

  def extract
    condition = true
    page = 1

    while condition do
      puts "page: #{page}"

      resp = HTTParty.get("http://challenge.dienekes.com.br:8888/api/numbers?page=#{page}").body
      numbers = JSON.parse(resp)['numbers']

      if numbers.blank?
        condition = false

        next
      end

      numbers.each { |n| @numbers << n }
      page += 1
      
    end
  end

  def transform
    0.upto(@numbers.count - 2) do |i|
      min = i
      
      (i+1).upto(@numbers.count - 1) do |j|
        min = j if @numbers[j] < @numbers[min]

        x = @numbers[i]
        @numbers[i] = @numbers[min]
        @numbers[min] = x
      end
    end
  end
end
