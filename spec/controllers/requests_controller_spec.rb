require 'rails_helper'

RSpec.describe RequestsController, type: :controller do
  describe "Testes do processo  de execução ETL" do
    it "EXTRACT" do
      get :index
      expect(response.status).to eq(200)
    end

    it "TRANSFORM" do
      expect([0.0002377087448300376]).to      include(0.0002377087448300376)
    end

    it "LOAD" do
      expect(response.body).to eq("")
    end
  end
end
